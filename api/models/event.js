var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var event_schema = new Schema({
  event: String,
  timestamps: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Event', event_schema); //Exporta o modelo e o seu schema para uso posterior no CRUD