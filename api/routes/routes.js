var express = require('express');
var router = express.Router();
var events_controller = require('../controllers/events_controller');

// Rotas
router.route('/events')
  .get(events_controller.show_events)
  .post(events_controller.create_event);

router.route('/events/:event_id')
  .get(events_controller.read_event)
  .put(events_controller.update_event)
  .delete(events_controller.delete_event);

module.exports = router;


// module.exports = function(app){
//   var events_controller = require('../controllers/events_controller');

//   // Rotas
//   app.route('/events')
//     .get(events_controller.show_events)
//     .post(events_controller.create_event);

//   app.route('/events/:event_id')
//     .get(events_controller.read_event)
//     .put(events_controller.update_event)
//     .delete(events_controller.delete_event);
// };