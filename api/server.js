var express = require('express');
var app = express(); // Definindo a aplicação 
var mongoose = require('mongoose');
var Event = require('./models/event');
var body_parser = require('body-parser');
var routes = require('./routes/routes');

// mongoose.Promise = global.Promise;
mongoose.connect('mongodb://diego:pass@ds129600.mlab.com:29600/events-api');

app.use(body_parser.urlencoded({extended: true}));
app.use(body_parser.json());

app.use('/api', routes);

app.listen(3000, function(){
  console.log("Aplicação executando em localhost:3000");
});